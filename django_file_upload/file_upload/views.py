from django.shortcuts import render
from django.http import HttpResponse
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.http import QueryDict
from rest_framework.views import APIView
from rest_framework.parsers import FormParser, MultiPartParser
import base64

from file_upload.models import Upload

# Create your views here.


class UploadView(APIView):
    def get(self, request):
        return HttpResponse('It works')

    def post(self, request):

        img_data = request.data.get('base64img')
        img_name = img_data['name']
        print(img_name)
        base64_img_str = img_data['base64string']

        img_upload = Upload()

        img_upload.uploaded_file = ContentFile(
            base64.b64decode(base64_img_str), name=img_name)
        img_upload.save()

        print('Saved image to disk')

        pdf_data = request.data.get('base64pdf')
        pdf_name = pdf_data['name']
        print(pdf_name)
        base64_pdf_str = pdf_data['base64string']

        pdf_upload = Upload()

        pdf_upload.uploaded_file = ContentFile(
            base64.b64decode(base64_pdf_str), name=pdf_name)
        pdf_upload.save()

        print('Saved pdf to disk')

        return HttpResponse(request.data)
