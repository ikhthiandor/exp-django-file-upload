# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-19 06:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('file_upload', '0002_auto_20161116_1156'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='upload',
            name='imageFile',
        ),
        migrations.RemoveField(
            model_name='upload',
            name='pdfFile',
        ),
        migrations.AddField(
            model_name='upload',
            name='uploaded_file',
            field=models.FileField(null=True, upload_to='/home/ikh/testube/ClonedRepos/exp-django-file-upload/django_file_upload/static/media'),
        ),
    ]
