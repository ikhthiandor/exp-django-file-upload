from rest_framework import serializers
from file_upload.models import Upload

class UploadSerializer(serializers.ModelSerializer):
    model = Upload
    read_only_fields = ('created',)
