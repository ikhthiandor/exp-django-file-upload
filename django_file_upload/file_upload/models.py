from django.db import models
from django.conf import settings
from django.core.files.storage import FileSystemStorage


class OverwriteStorage(FileSystemStorage):

    def get_available_name(self, name, max_length):
        self.delete(name)
        return name

class Upload(models.Model):
    uploaded_file = models.FileField(max_length=200, upload_to=settings.MEDIA_ROOT, null=True, storage=OverwriteStorage())
    created_at = models.DateTimeField(auto_now_add=True)
