from django.conf.urls import url
from django.contrib import admin

from file_upload.views import UploadView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^upload/', UploadView.as_view())
]
